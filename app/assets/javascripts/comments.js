document.addEventListener('turbolinks:load', () => {
    const form = document.querySelector("#comment_form");
    if (!form) return;

    form.addeEventListener('ajax:success', (event) => {
        console.log("Evento", event);
        const comment = event.detail[0];
        console.log("Objeto", comment);

        const row = document.createElement('tr');
        const data = document.createElement('td');
        data.innerTextHTML = comment.tweet;
        row.appendChild(data);

        const table = document.querySelector("#comment_table");
        table.appendChild(row);
    });

});
