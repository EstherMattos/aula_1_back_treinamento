json.extract! comment, :id, :tweet, :created_at, :updated_at
json.url comment_url(comment, format: :json)
